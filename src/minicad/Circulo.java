
package minicad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author rafa_
 */
public class Circulo extends Figura {
    Point punto1;
    Point punto2;
    int radio,diametro;
    Color color;
    
    
    Circulo (Point p1,Point p2, Color col){
        punto1 = new Point(p1.x,p1.y);
        punto2 = new Point(p2.x,p2.y);
        radio = (int)punto1.distance(punto2.x,punto2.y);
        diametro = radio*2;
        color = col;
    }
    
    void dibujar(Graphics2D g2d){
        if (this.color!=null){
            g2d.setColor(color);
        }        
        g2d.drawOval(punto1.x-radio,punto1.y-radio, diametro, diametro);
        g2d.fillOval(punto1.x-radio,punto1.y-radio, diametro, diametro);
   }
}
