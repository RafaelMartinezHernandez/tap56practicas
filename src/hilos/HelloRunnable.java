/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

public class HelloRunnable implements Runnable {
    String nombre;
    int tiempo;
    public HelloRunnable(String _nombre, int _tiempo){
        nombre = _nombre;
        tiempo = _tiempo;
    }
    public void run() {
        for(int i=0;i<100;i++){
            try {
                System.out.println("Hello from a thread "+nombre+", contando: "+i);
                Thread.sleep(tiempo);                
            } catch (InterruptedException ex) {
                Logger.getLogger(HelloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }                
    }

    public static void main(String args[]) {
        (new Thread(new HelloRunnable("Pancho",1000))).start();
        (new Thread(new HelloRunnable("AndrehManuel",3000))).start();
    }

}
